import glob
import logging
import os

from utils import createFolderForNewDataset, renameAllVariablesFor, ThreadForRenameAllVariablesFor

UNSAFE = 'unsafe'

SAFE = 'safe'

logging.basicConfig(format="%(levelname)s - %(asctime)s - %(filename)s/%(funcName)s - line %(lineno)d: %(message)s",
                        datefmt='%H:%M:%S', level=logging.INFO)

TEST = False


new_path_dataset = "/absolute/path/"
if TEST :
    new_path_dataset = "/absolute/path/test/"

rules_templates = "15136-r3-r4_11"
# rules_templates = "19264-r0-r1-r2-r5_14"

nameDataset = "HOP-mini-dataset-%s-template" % rules_templates
if TEST :
    nameDataset = "data-test"


new_dataset_name = "HOP-renamedVariable-data-%s-template-with-HTML-for-D1/" % rules_templates
if TEST :
    new_dataset_name = "data-test-final"

createFolderForNewDataset(pathname=new_path_dataset, datasetname=new_dataset_name)
new_root_dataset_path = os.path.join(new_path_dataset, new_dataset_name)

root_dataset_path = "/absolute/path/%s/" % nameDataset
if TEST :
    root_dataset_path = "/absolute/path//test/%s/" % nameDataset


# #TODO Improve the speed to this script -- do it with 2 thread one for safe part and the other one for unsafe
list_dataset = glob.glob(os.path.join(root_dataset_path, '*',''), recursive=False)
logging.info(root_dataset_path)
logging.info(list_dataset)

logging.debug("BEGINING OF THE PROCESS")
count = 1
for root, name, filenames in os.walk(root_dataset_path):
    logging.debug(name)
    logging.debug(root_dataset_path)
    if ".directory" in filenames:
        filenames.remove(".directory")
    if os.path.join(nameDataset, SAFE) in root:
        logging.debug("Process safe file")
        # logging.debug(filenames)
        count = renameAllVariablesFor(count, filenames, root, SAFE, new_root_dataset_path)
    elif os.path.join(nameDataset, UNSAFE) in root:
        logging.debug("Process unsafe file")
        # logging.debug(filenames)
        count = renameAllVariablesFor(count, filenames, root, UNSAFE, new_root_dataset_path)

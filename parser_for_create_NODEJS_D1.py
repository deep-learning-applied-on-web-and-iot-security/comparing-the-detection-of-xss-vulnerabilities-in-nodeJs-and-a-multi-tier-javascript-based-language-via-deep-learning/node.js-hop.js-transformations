import logging
import os

from utils import createFolderForNewDataset, renameAllVariablesFor

UNSAFE = 'unsafe'

SAFE = 'safe'

logging.basicConfig(format="%(levelname)s - %(asctime)s - %(filename)s/%(funcName)s - line %(lineno)d: %(message)s",
                        datefmt='%H:%M:%S', level=logging.INFO)

new_path_dataset = "/absolute/path/"

rules_templates = "r3-r4_11"
rules_templates = "r0-r1-r2-r5_14"
nameDataset = "NODEJS-mini-dataset-%s-template" % rules_templates

new_dataset_name = "NODEJS-renamedVariable-data-%s-template-with-HTML-for-D1/" % rules_templates
createFolderForNewDataset(pathname=new_path_dataset, datasetname=new_dataset_name)
new_root_dataset_path = os.path.join(new_path_dataset, new_dataset_name)

root_dataset_path = "/absolute/path/%s/" % nameDataset

count = 1

#TODO Improve the speed to this script -- do it with 2 thread one for safe part and the other one for unsafe
for root, name, filenames in os.walk(root_dataset_path):
    logging.debug(root_dataset_path)
    if ".directory" in filenames:
        filenames.remove(".directory")
    if os.path.join(nameDataset, SAFE) in root:
        logging.debug("Process safe file")
        # logging.debug(filenames)
        count = renameAllVariablesFor(count, filenames, root, SAFE, new_root_dataset_path)
    elif os.path.join(nameDataset, UNSAFE) in root:
        logging.debug("Process unsafe file")
        # logging.debug(filenames)
        count = renameAllVariablesFor(count, filenames, root, UNSAFE, new_root_dataset_path)
